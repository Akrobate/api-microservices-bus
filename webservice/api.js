var SenecaWeb = require('seneca-web')
var Express = require('express')
var webserviceConf = require('./conf/webservice')
var Router = Express.Router
var context = new Router()

// Web service conf
conf = new webserviceConf()

var apiController = require('./controller/api')

var senecaWebConfig = {
    context: context,
    adapter: require('seneca-web-adapter-express'),
    options: { parseBody: false } // so we can use body-parser
}

// Configuring and starting express
var app = Express()
.use( require('body-parser').json() )
.use( context )
.listen(conf.api_port)

// Seneca init and binding with web
var seneca = require('seneca')()
.use('seneca-amqp-transport')
.use(SenecaWeb, senecaWebConfig )

// Nothing interresing
// .client()

// Conf connection in direct way { port:9003, pin:'role:users', host:'0.0.0.0' }
// .client( { port:9003, pin:'role:users', host:'192.168.1.14' } )
// .client( { port:9002,  pin:'role:message' } )

// Ultimate prod mabbitMQ + AMQP messaging multi topics queue
//https://www.npmjs.com/package/seneca-amqp-transport
.client({
  type: 'amqp',
  //pin: 'cmd:log,level:*',
  //pin:'role:users,cmd:*',
  pin: 'role:*',
  url: conf.getBusAmqpUrl()
})
.use(apiController)
