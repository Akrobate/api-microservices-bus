module.exports = function microservicesRoutes(options) {


    this.add('role:api,path:users/getall', function (msg, respond) {
        console.log("users/getall -> role:users,cmd:getall")
        this.act('role:users,cmd:getall', respond)
    })


    this.add('role:api,path:users/countmessages', function (msg, respond) {
        console.log("users/getall -> role:users,cmd:countmessages")
        this.act('role:users,cmd:countmessages', respond)
    })


    this.add('role:api,path:users', function (msg, respond) {
        var valid_ops = { me:'me', countmessages2:'countmessages2' }
        var action = msg.args.params.action
        console.log("users/" + valid_ops[action] + " -> role:users,cmd:" + valid_ops[action] )
        //var left = msg.args.query.mavargeturl   // access get vars
        if (valid_ops[action] !== undefined) {
            this.act('role:users', {
                cmd:   valid_ops[action],
            }, respond)
        } else {
            respond()
        }
    })


    this.add('role:api,path:messages', function (msg, respond) {
        var valid_ops = { read:'read',count:'count', longoperation: 'longoperation' }
        var action = msg.args.params.action
        console.log("messages/" + valid_ops[action] + " -> role:users,cmd:" + valid_ops[action] )
        if (valid_ops[action] !== undefined) {
            this.act('role:message', {
                cmd:   valid_ops[action],
            }, respond)
        } else {
            respond;
        }
    })

    this.add('role:api,path:messages/useless', function (msg, respond) {
        console.log("messages/useless -> nothing")
        respond()
    })
}
