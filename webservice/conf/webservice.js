module.exports = function webserviceConf(options) {
    /**
     * [External api port number]
     * @type {Int}
     */
    this.api_port = 3001;

    /**
     * [microservice bus connection url]
     * @type {String}
     */
    this.amqp_url = "amqp://guest:guest@localhost:5673/seneca?locale=fr_FR";
    this.default_bus_port = 5673;
    this.default_bus_host = "localhost";
    this.default_bus_virutal_host = "/";
    
    this.getBusPort = () => {
        var port = this.default_bus_port
        if (process.env.BUS_PORT !== undefined) {
            port = process.env.BUS_PORT
        }
        return port
    }

    this.getBusHost = () => {
        var host = this.default_bus_host
        if (process.env.BUS_HOST !== undefined) {
            host = process.env.BUS_HOST
        }
        return host
    }

    this.getBusVirtualHost = () => {
        var vhost = this.default_bus_virutal_host
        if (process.env.BUS_VHOST !== undefined) {
            vhost = process.env.BUS_VHOST
        }
        return vhost
    }

    this.getBusAmqpUrl = () => {
        var host = this.getBusHost()
        var vhost = this.getBusVirtualHost()
        var port = this.getBusPort()
        return "amqp://guest:guest@" + host + ":" + port + vhost + "?locale=fr_FR";
    }
}
