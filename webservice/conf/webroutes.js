module.exports = function webRoutes(options) {

    this.add('init:api', function (msg, respond) {
        this.act('role:web',{routes:[
            {
                prefix: 'api/v1',
                pin:    'role:api,path:*',
                map: {
                    'users/getall': { GET:true },
                    'users/countmessages': { GET:true },
                    'users': { GET:true, suffix:'/:action' },
                    'messages/useless': { GET:true },
                    'messages': { GET:true, suffix:'/:action' },
                }
            },
            {
                prefix: 'api/v2',
                pin:    'role:api,path:*',
                map: {
                    'users': { GET:true, suffix:'/:action' },
                    'messages/useless': { GET:true },
                    'messages': { GET:true, suffix:'/:action' },
                }
            }
        ]}, respond)
    })
}
