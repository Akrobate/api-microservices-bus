var microservicesRoutes = require('../conf/microserviceroutes')
var webRoutes = require('../conf/webroutes')

/**
 * [exports description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
module.exports = function api(options) {

    this.use(microservicesRoutes)
    this.use(webRoutes)
}
