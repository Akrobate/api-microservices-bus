/**
 * [exports description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
module.exports = function externalBusinessLogic(options) {

    this.transport_handler = null;

    /**
     * [setTransportHandler description]
     * @param {[type]} handler [description]
     */
    this.setTransportHandler = function(handler) {
        this.transport_handler = handler
    }

    /**
     * [getCountMessages description]
     * @param  {[type]}   params   [description]
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    this.getCountMessages = function(params, callback) {
        console.log("User business logic executing external businesslogic: getCountMessages");
        this.transport_handler.act('role:message,cmd:count', function(err, data) {
            console.log("Data returned from 'role:message,cmd:count'")
            console.log(data)
            callback(data)
        })
    }
}
