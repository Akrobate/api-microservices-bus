var userBusinessLogic = require('../businesslogic/users')
var externalBusinessLogic = require('../businesslogic/external/messages')

var external_logic = new externalBusinessLogic()
var local_logic = new userBusinessLogic()

local_logic.setExternalLogic(external_logic)

/**
 * [Simple module to glue the reusts to business rules]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
module.exports = function usersController(options) {

    // Wrap external communication
    external_logic.setTransportHandler(this)

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:users,cmd:getall', (msg, respond) => {
        console.log("role:users,cmd:getall")
        // Must be explicit params mapping
        params = {}
        params = msg
        local_logic.getAll(params, (err, data) => {
            respond(err, { users: data })
        });
    })

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:users,cmd:me', (msg, respond) => {
        console.log("role:users,cmd:countmessages => empty logic exemple")
        respond(null, { answer: 'John Doe' })
    })

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:users,cmd:countmessages', (msg, respond) => {
        console.log("role:users,cmd:countmessages");
        var params = {}
        //params.id = msg.id
        local_logic.countUserMessages(params, (result) => {
            respond(null, result)
        })
    })
}
