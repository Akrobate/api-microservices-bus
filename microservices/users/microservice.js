var usersController = require('./controller/userscontroller')
var microserviceConf = require('./conf/microservice')

var conf = new microserviceConf()

console.log("connection url " + conf.getBusAmqpUrl())

require('seneca')()
.use('seneca-amqp-transport')
//.client( { port: 9002,  pin:'role:message' } )

.client({
    type: 'amqp',
    //pin: 'cmd:log,level:*',
    //pin:'role:users,cmd:*',
    pin: 'role:*',
    url: conf.getBusAmqpUrl()
})

//.act('role:math,cmd:sum,left:5,right:2', console.log)
//.listen()
//.listen({ port: 9003 })
.listen({
    type: 'amqp',
    //pin: 'cmd:log,level:*',
    url: conf.getBusAmqpUrl(),
    pin: 'role:users'
})

.use(usersController)
