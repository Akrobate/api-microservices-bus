var messageController = require('./controller/messagecontroller')
var microserviceConf = require('./conf/microservice')

var conf = new microserviceConf()

require('seneca')()
.use('seneca-amqp-transport')
.use(messageController)
//.listen()
//.listen({ port: 9002 })

// Ultimate prod mabbitMQ + AMQP messaging multi topics queue
//https://www.npmjs.com/package/seneca-amqp-transport

.listen({
    type: 'amqp',
    url: conf.getBusAmqpUrl(),
    pin: 'role:message'
})
