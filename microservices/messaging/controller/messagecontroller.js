module.exports = function messageConroller(options) {

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:message,cmd:read', function(msg, respond) {
        console.log("role:message,cmd:read")
        var message = "The message you were expecting from, comes right from microservice"
        respond(null, { 'message': message })
    })

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:message,cmd:write', function product(msg, respond) {
        console.log("role:message,cmd:write")
        console.log("I was supposed to store this message \n: " + msg.message_content);
        respond(null, { 'status':'messagestored' })
    })

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:message,cmd:count', function(msg, respond) {
        console.log("role:message,cmd:count")
        respond(null, { 'count': 777 })
    })

    /**
     * [role description]
     * @type {[type]}
     */
    this.add('role:message,cmd:longoperation', function(msg, respond) {
        console.log("role:message,cmd:longoperation")
        setTimeout(function(){
            respond(null, { 'longoperation': 'done' })
        }, 3000)

    })
}
